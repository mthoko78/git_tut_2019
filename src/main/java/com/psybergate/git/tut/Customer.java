package com.psybergate.git.tut;

import java.util.Date;

public class Customer {

	private long customerNum;

	private String name;

	private String surname;

	private Date dateOfBirth;

	public Customer() {

	} 

	public Customer(long customerNum, String name, String surname, Date dateOfBirth) {
		this.customerNum = customerNum;
		this.name = name;
		this.surname = surname;
		this.dateOfBirth = dateOfBirth;
	}

	public long getCustomerNum() {
		return customerNum;
	}

	public void setCustomerNum(long customerNum) {
		this.customerNum = customerNum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@Override
	public String toString() {
		return String.format("[customerNum=%s, name=%s, surname=%s, dateOfBirth=%s]", customerNum, name, surname, dateOfBirth);
	}

}